import './main.scss';

console.log("test");

/**
 * Notes
 * 
 * CSS/SCSS Animation
 * 
 * -- Transform
 * -- Transitions
 * -- Keyframes
 * -- Animation basic
 * -- Web Examples
 */

 // TRANSFOR

 // transform: 
    // Translate
    // translate(200px) -- move along the x-axis to the right
    // translateX(-200px) -- move along the x-axis to the left
    // translateY(200px) -- move along the x-axis to the bottom
    // translateY(-200px) -- move along the x-axis to the top
    // translate(-200px, 200px) -- move along the x-axis to the top (x, y)
    //--------------------------------------------------------------------
    // Scale
    // scaleX() -- positive number will stretch the image along the x-axis
    //          -- negative number will shrink the image along the x-axis
    // scaleY() -- positive number will stretch the image along the y-axis
    //          -- negative number will shrink the image along the y-axis
    // scale(x, y) -- positive number will stretch the image
    //             -- negative number will shrink the image
    //--------------------------------------------------------------------
    // Rotate
    // rotateX() --e.g 10deg number rotate along the x-axis
    // rotateY() --e.g 10deg number rotate along the Y-axis
    // rotateZ() --e.g rotate 0deg to 360deg

// Animation Basic 
   // -- Resource (https://cubic-bezier.com/)

const addToCartBtnAll = document.querySelectorAll('.add-to-cart')!;
addToCartBtnAll.forEach(function(button, key) {
   button.addEventListener('click', function() {
      const itemImg = button.closest('li')!
                            .querySelector('img')!;
      let cloneImg = itemImg.cloneNode(true) as HTMLImageElement;
      cloneImg.classList.add('zoom');
      document.querySelector('body')?.append(cloneImg);
      setTimeout(() => {cloneImg.remove()}, 1000);
   });
});